package go.alex.webfluxsecurity.entity;

public enum UserRole {
    USER,
    ADMIN
}
